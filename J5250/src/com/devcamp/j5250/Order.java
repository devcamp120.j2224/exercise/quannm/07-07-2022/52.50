package com.devcamp.j5250;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Order {
    public int id; // id của order
    public String customerName; // tên khách hàng
    public long price;// tổng giá tiền
    public Date orderDate; // ngày thực hiện order
    public boolean confirm; // đã xác nhận hay chưa?
    public String[] items; // danh sách mặt hàng đã mua

    // Khởi tạo 0 tham số
    public Order() {
        this.id = 1108;
        this.customerName = "QuanNM";
        this.price = 200000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"Book", "Pencil"};
    }

    // Khởi tạo 2 tham số
    public Order(int id, String customerName) {
        this.id = id;
        this.customerName = customerName;
        this.price = 150000;
        this.orderDate = new Date();
        this.confirm = false;
        this.items = new String[] {"Rule", "Pencil"};
    }

    // Khởi tạo 3 tham số
    public Order(int id, String customerName, Date orderDate) {
        this(id, customerName);
        this.orderDate = orderDate;
        this.price = 150000;
        this.confirm = false;
        this.items = new String[] {"Pizza", "Burger"};
    }

    // Khởi tạo 6 tham số
    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this(id, customerName, orderDate);
        this.price = price;
        this.confirm = confirm;
        this.items = items;
    }

    @Override
    public String toString() {
        // Định dạng tiêu chuẩn VN
        Locale.setDefault(new Locale("vi", "VN"));
        // Định dạng cho ngày tháng
        String pattern = "dd-MM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaultTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        // Định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        // return string
        return 
            "Order [id=" + id
            + ", customerName=" + customerName
            + ", price=" + usNumberFormat.format(price)
            + ", orderDate=" + defaultTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
            + ", confirm=" + confirm
            + ", items=" + Arrays.toString(items);
    }
}
