import java.util.ArrayList;
import java.util.Date;

import com.devcamp.j5250.Order;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        ArrayList<Order> listOrders = new ArrayList<Order>();

        // Tạo các order
        Order order1 = new Order();
        Order order2 = new Order(0306, "BoiHB");
        Order order3 = new Order(2005, "ThuNHM", new Date());
        Order order4 = new Order(3112, "DucNM", 200000, new Date(), true, new String[] {"Chicken", "Pork", "Beef"});

        // Thêm Object vào danh sách
        listOrders.add(order1);
        listOrders.add(order2);
        listOrders.add(order3);
        listOrders.add(order4);

        // In ra
        for (Order order : listOrders) {
            System.out.println(order.toString());
        }
    }
}
